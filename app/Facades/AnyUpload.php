<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\ExtendClass\AnyUpload
 *
 * @method static config($config, $fileField = 'file', $method_type = "upload")
 * @method static createThumb($info)
 * @method static getFileInfo()
 * @method static extToType()
 * @method static deletePath()
 * @method static size()
 *
 * Class AnyUpload
 * @package App\Facades\AnyUpload
 */
class AnyUpload extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'any_upload';
    }
}
